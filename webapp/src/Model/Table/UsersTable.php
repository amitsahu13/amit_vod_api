<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
            
        $validator
            ->requirePresence('firstname', 'create')
            ->notEmpty('firstname');
            
        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname');
            
        $validator
           // ->add('dob', 'valid', ['rule' => 'date'])
                ->allowEmpty('dob', 'create');
//            ->requirePresence('dob', 'create');
//            
            //->notEmpty('dob');
            
        $validator
                ->allowEmpty('mob_no', 'create');
            //->requirePresence('mob_no', 'create');
            //->notEmpty('mob_no');
            
        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'Username is should be unique']);
            
        $validator
            ->add('password', ['length' => ['rule' => ['minLength', 8],
            'compare' => [
            'rule' => ['compareWith', 'cpassword']
             ],
            'message' => 'Password need to be at least 8 characters long',]])
            ->requirePresence('password', 'create')
            ->notEmpty('password');
            
        $validator
            ->add('email', 'valid', ['rule' => 'email','message' => 'E-mail must be valid'])
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table','message'=>'You are already registerd with us']);    
            
        $validator
            ->requirePresence('role', 'create')
            ->notEmpty('role');
        $validator
         ->notEmpty('cpassword')
         ->add('cpassword', ['length' => ['rule' => ['minLength', 8],'message' => 'Password need to be at least 8 characters long',]])
         ->add('cpassword', 'passwordsEqual', [
                    'rule' => function($value, $context) {
                        return isset($context['data']['cpassword']) &&
                         $context['data']['password'] === $value;      
                    },
                    'message' => 'The password & confirm password are not equal',
            ]);    
            
//        $validator
//            ->allowEmpty('fblink');
            
        $validator
            //->requirePresence('address', 'create');
             ->allowEmpty('address', 'create');
           // ->notEmpty('address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
