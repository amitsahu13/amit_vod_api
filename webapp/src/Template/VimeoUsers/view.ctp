<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Vimeo User'), ['action' => 'edit', $vimeoUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vimeo User'), ['action' => 'delete', $vimeoUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vimeoUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Vimeo Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vimeo User'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="vimeoUsers view large-10 medium-9 columns">
    <h2><?= h($vimeoUser->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Cid') ?></h6>
            <p><?= h($vimeoUser->cid) ?></p>
            <h6 class="subheader"><?= __('Client Secret') ?></h6>
            <p><?= h($vimeoUser->client_secret) ?></p>
            <h6 class="subheader"><?= __('Client Access Token') ?></h6>
            <p><?= h($vimeoUser->client_access_token) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($vimeoUser->id) ?></p>
        </div>
    </div>
</div>
